package com.example.contentproviderleroy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class deleteWordActivity extends AppCompatActivity {

    private ArrayAdapter<String> arrayAdapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_word);
        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_multiple_choice);
        listView = findViewById(R.id.listViewDelete);
        listView.setAdapter(arrayAdapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        loadWordsToDelete();
    }

    private void loadWordsToDelete() {
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(
                UserDictionary.Words.CONTENT_URI,
                new String[]{UserDictionary.Words._ID, UserDictionary.Words.WORD},
                null,
                null,
                null
        );

        if (cursor != null) {
            try {
                int wordIndex = cursor.getColumnIndex(UserDictionary.Words.WORD);

                while (cursor.moveToNext()) {
                    String word = cursor.getString(wordIndex);
                    arrayAdapter.add(word);
                }
            } finally {
                cursor.close();
            }
        }
    }

    public void validate(View v){
        SparseBooleanArray checkedItems = listView.getCheckedItemPositions();
        ArrayList<String> wordsToDelete = new ArrayList<>();

        for (int i = 0; i < checkedItems.size(); i++) {
            int position = checkedItems.keyAt(i);
            if (checkedItems.valueAt(i)) {
                String wordToDelete = arrayAdapter.getItem(position);
                wordsToDelete.add(wordToDelete);
            }
        }

        // Utilisez un Intent pour renvoyer la liste des mots à supprimer à MainActivity
        Intent resultIntent = new Intent();
        resultIntent.putStringArrayListExtra("wordsToDelete", wordsToDelete);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    public void cancel(View v){
        setResult(RESULT_CANCELED);
        finish();
    }
}