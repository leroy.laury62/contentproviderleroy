package com.example.contentproviderleroy;

import androidx.appcompat.app.AppCompatActivity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import java.net.URI;
import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private ArrayAdapter<String> arrayAdapter;
    private Switch orderSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        orderSwitch = findViewById(R.id.order);
        orderSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> showWords(buttonView));

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        ListView listView = findViewById(R.id.listView);
        listView.setAdapter(arrayAdapter);
        showWords(listView);
    }

    public void showWords(View v) {

        ContentResolver contentResolver = getContentResolver();
        boolean isAscOrder = orderSwitch.isChecked();
        String sortOrder = isAscOrder ? " ASC" : " DESC";
        Cursor cursor = contentResolver.query(
                UserDictionary.Words.CONTENT_URI,
                new String[]{UserDictionary.Words._ID, UserDictionary.Words.WORD},
                null,
                null,
                UserDictionary.Words.WORD + sortOrder
        );

        if (cursor != null) {
            try {
                int wordIndex = cursor.getColumnIndex(UserDictionary.Words.WORD);

                //clear listView
                arrayAdapter.clear();

                while (cursor.moveToNext()) {
                    String word = cursor.getString(wordIndex);
                    arrayAdapter.add(word);
                }
            } finally {
                cursor.close();
            }
        } else {
            // Gestion de l'erreur si la requête échoue
            Toast.makeText(this, "Erreur lors de l'accès au dictionnaire utilisateur", Toast.LENGTH_SHORT).show();
        }
    }

    public void addWord(View v){
        // Lancez l'activité addWordActivity
        startActivityForResult(new Intent(this, addWordActivity.class), 1);
    }

    public void deleteWord(View v){
        // Lancez l'activité deleteWordActivity
        startActivityForResult(new Intent(this, deleteWordActivity.class), 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && data != null && data.hasExtra("newWord")) {
            // Récupérez le nouveau mot du Intent
            String newWord = data.getStringExtra("newWord");
            //vérifiez que le mot n'existe pas déjà
            if (arrayAdapter.getPosition(newWord) >= 0) {
                Toast.makeText(this, "Le mot existe déjà", Toast.LENGTH_SHORT).show();
                return;
            }
            // Ajoutez le nouveau mot à la liste
            arrayAdapter.add(newWord);
            // Ajoutez le nouveau mot au dictionnaire utilisateur
            ContentResolver contentResolver = getContentResolver();
            ContentValues contentValues = new ContentValues();
            contentValues.put(UserDictionary.Words.WORD, newWord);
            contentValues.put(UserDictionary.Words.LOCALE, Locale.getDefault().toString());

            Uri uri = contentResolver.insert(UserDictionary.Words.CONTENT_URI, contentValues);
            if (uri != null) {
                Toast.makeText(this, "Le mot a été ajouté au dictionnaire utilisateur", Toast.LENGTH_SHORT).show();
                showWords(null);
            } else {
                Toast.makeText(this, "Erreur lors de l'ajout du mot au dictionnaire utilisateur", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == 2 && resultCode == RESULT_OK) {
            if (data != null) {
                ArrayList<String> wordsToDelete = data.getStringArrayListExtra("wordsToDelete");
                if (wordsToDelete != null && !wordsToDelete.isEmpty()) {
                    ContentResolver contentResolver = getContentResolver();

                    for (String word : wordsToDelete) {
                        String selection = UserDictionary.Words.WORD + "=?";
                        String[] selectionArgs = {word};
                        contentResolver.delete(UserDictionary.Words.CONTENT_URI, selection, selectionArgs);
                    }
                    showWords(findViewById(R.id.listView));
                }
            }
        }
    }
}