package com.example.contentproviderleroy;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class addWordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_word);
    }

    public void validate(View v){
        String newWord = ((EditText) findViewById(R.id.addWordText)).getText().toString();
        if (!newWord.isEmpty()) {
            // Envoyez le nouveau mot à l'activité principale
            setResult(RESULT_OK, getIntent().putExtra("newWord", newWord));
            finish();
        }
    }

    public void cancel(View v){
        setResult(RESULT_CANCELED);
        finish();
    }
}